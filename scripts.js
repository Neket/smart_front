// Инициализируем графики, после только конфиги меняем
var chart_line = Highcharts.chart('graph-line', {

    title: {
        text: 'График посещений зала'
    },

    yAxis: {
        title: {
            text: 'Количество посетителей'
        }
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false,
                
            }
        },
        line: {
            dataLabels: {
                enabled: true,
                inside: false
            }
        }
    },

    series: [{
        name: 'Максимум',
        data: []
    }, {
        name: 'Средняя',
        data: []
    }, {
        name: 'Минимум',
        data: []
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

var chart_circle = Highcharts.chart('graph-zone', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Текущая загруженность зон'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}',
                connectorColor: 'silver'
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Зоны',
        colorByPoint: true,
        data: [ 
        {
            name: 'Зона 1',
            y: 0
        }, {
            name: 'Зона 2',
            y: 0
        }, {
            name: 'Зона 3',
            y: 0
        }, {
            name: 'Зона 4',
            y: 0
        }
        ]
    }]
});

 $('#gr-date').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
});

site_api = 'http://test-video-ai.unix.tensor.ru:8000';

var 
    gr_zone = document.getElementById('gr-zone'),
    gr_interval = document.getElementById('gr-interval'),
    gr_date = document.getElementById('gr-date');

// Заполняем селект зонами
var xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        var resp = JSON.parse(this.responseText);
        for(var i = 0; i < resp.length;i++){
            var opt = document.createElement('option');
            opt.value = resp[i]['id'];
            opt.innerHTML = resp[i]['name'];
            gr_zone.appendChild(opt);
        }
    }
});
xhr.open("GET", site_api + "/zone/getall");
xhr.send();

var reloadGraph = function() { 
    var 
        data = new FormData(),
        xhr = new XMLHttpRequest();

    data.append("interval", gr_interval.options[gr_interval.selectedIndex].value);
    data.append("date", gr_date.value);
    data.append("zone", gr_zone.value);
        
    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            var resp = JSON.parse(this.responseText);  
            chart_circle.update({series: [{data: resp['zones']}]});
            chart_line.update({
                series: [{
                    name: 'Максимум',
                    data: resp['max']
                }, {
                    name: 'Средняя',
                    data: resp['middle']
                }, {
                    name: 'Минимум',
                    data: resp['min']
                }],
                xAxis: {
                    categories: resp['period']
                }
            });
        }
    });
    xhr.open("POST", site_api + "/stat/charts");
    xhr.send(data);    
}

setInterval(reloadGraph, 1000);

